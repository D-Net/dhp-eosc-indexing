# dhp-eosc-indexing

This project implements the OpenAIRE Graph indexing for the EOSC search portal. More in details it
* identifies the content to be processed (entities/relations)
* maps the data to the agreed data model
* creates the Solr documents according to the agreed Solr schema specification
* pushes the Solr documents to the server